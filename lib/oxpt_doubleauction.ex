defmodule Oxpt.Doubleauction do
  use Cizen.Automaton
  defstruct [:room_id]

  use Cizen.Effects
  alias Cizen.{Dispatcher, Event, Filter}
  alias Oxpt.JoinGame
  alias Oxpt.Doubleauction.Host
  alias Oxpt.Game
  alias Oxpt.Player.{Input, Output}

  @root_dir Path.join(__ENV__.file, "../..") |> Path.expand()

  @behaviour Oxpt.Game
  @impl Oxpt.Game
  def build_assets(out_dir, public_url) do
    opts = [into: IO.stream(:stdio, :line), cd: @root_dir]
    System.cmd("npm", ["install", "--save-dev", "parsel"], opts)
    System.cmd("npm", ["run", "build", "--out-dir", out_dir, "--public-url", public_url], opts)
  end

  @impl Oxpt.Game
  def watch_assets(out_dir, public_url) do
    opts = [into: IO.stream(:stdio, :line), cd: @root_dir]
    System.cmd("npm", ["install", "--save-dev", "parsel"], opts)
    Game.run_watcher(["node", "./node_modules/parcel-bundler/bin/cli.js", "watch", "src/index.jsx", "--out-dir", out_dir, "--public-url", public_url], opts)
  end

  @impl Oxpt.Game
  def player_socket(game_id, game, guest_id) do
    %__MODULE__.PlayerSocket{game_id: game_id, guest_id: guest_id}
  end

  @impl Oxpt.Game
  def new(room_id, _params) do
    %__MODULE__{room_id: room_id}
  end

  @impl true
  def spawn(id, %__MODULE__{room_id: room_id}) do
    host = perform id, %Start{
      saga: Host.new(room_id, [game_id: id])
    }
    perform id, %Subscribe{
      event_filter: Filter.new(fn %Event{body: %JoinGame{
        game_id: ^id, host: true
      }} -> true end)
    }

    {:loop, host, %{}}
  end

  @impl true
  def yield(id, {:loop, host, state}) do
    event = perform id, %Receive{}
    {new_host, new_state} = case event.body do
      %JoinGame{host: true, guest_id: guest_id} ->
        perform id, %Request{
          body: %JoinGame{game_id: host, guest_id: guest_id, host: true}
        }
        {host, state}
    end
    {:loop, new_host, new_state}
  end
end

