defmodule Oxpt.Doubleauction.PlayerSocket do
  use Cizen.Automaton
  defstruct [:game_id, :guest_id]

  use Cizen.Effects

  alias Cizen.{Event, Filter}
  alias Oxpt.Player.{Input, Output}

  @impl true
  def spawn(id, %__MODULE__{} = socket) do
    perform id, %Subscribe{
      event_filter: Filter.new(fn %Event{body: %Input{
        game_id: game_id,
        guest_id: guest_id
      }} ->
        game_id == socket.game_id and guest_id == socket.guest_id
      end)
    }

    {:loop, socket}
  end

  @impl true
  def yield(id, {:loop, socket}) do
    event = perform id, %Receive{
      event_filter: Filter.new(fn %Event{body: %Input{}} -> true end)
    }

    case event.body do
      %Input{event: "click", payload: %{"current" => value}} ->
        perform id, %Dispatch{
          body: %Output{
            game_id: socket.game_id,
            guest_id: socket.guest_id,
            event: "update",
            payload: %{count: value + 1}
          }
        }
    end

    {:loop, socket}
  end
end

