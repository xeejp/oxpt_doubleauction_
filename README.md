# Oxpt.Doubleauction

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `oxpt_doubleauction` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:oxpt_doubleauction, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/oxpt_doubleauction](https://hexdocs.pm/oxpt_doubleauction).

