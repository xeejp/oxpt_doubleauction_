import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardHeader from '@material-ui/core/CardHeader'
import CircularProgress from '@material-ui/core/CircularProgress'
import { ReadJSON, InsertVariable } from '../util/ReadJSON'

const mapStateToProps = ({ userNumber }) => ({ userNumber })

class Wait extends Component {
  render() {
    const { userNumber } = this.props
    return (
      <div>
        <Card>
          <CardHeader title={ReadJSON().static_text['title']} subtitle={ReadJSON().static_text['waiting'][0]} />
          <CardContent>
            <p>{ReadJSON().static_text['waiting'][1]}</p>
            <p>{ReadJSON().static_text['waiting'][2]}</p>
            <p>{InsertVariable(ReadJSON().static_text['waiting'][3], { user_number: userNumber })}</p>
          </CardContent>
          <div style={{ textAlign: 'center' }}>
            <CircularProgress size={140} thickness={5.0} />
          </div>
        </Card>
      </div>
    )
  }
}

Wait.propTypes = {
  userNumber: PropTypes.number.isRequired,
}

export default connect(mapStateToProps)(Wait)
