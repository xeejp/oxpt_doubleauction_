import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardHeader from '@material-ui/core/CardHeader'

import { ReadJSON, InsertVariable, LineBreak } from '../util/ReadJSON'

const mapStateToProps = ({ personal, dynamicText }) => ({
  role: personal.role,
  money: personal.money,
  dynamicText: dynamicText,
})

class Description extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    const { role, money, dynamicText } = this.props
    return (
      <Card>
        <CardHeader title={ReadJSON().static_text['title']} subtitle={dynamicText['desc'][0]} />
        <CardContent>
          <p>{LineBreak(InsertVariable(dynamicText['desc'][1], {}, dynamicText['variables']))}</p>

          {role === 'buyer'
            ? <p>{LineBreak(InsertVariable(dynamicText['desc'][2], { money: money }, dynamicText['variables']))}</p>
            : <p>{LineBreak(InsertVariable(dynamicText['desc'][3], { money: money }, dynamicText['variables']))}</p>
          }
          <p>{LineBreak(InsertVariable(dynamicText['desc'][4], {}, dynamicText['variables']))}</p>
        </CardContent>
      </Card>
    )
  }
}

Description.propTypes = {
  role: PropTypes.string.isRequired,
  money: PropTypes.number.isRequired,
  dynamicText: PropTypes.string.isRequired
}

export default connect(mapStateToProps)(Description)
