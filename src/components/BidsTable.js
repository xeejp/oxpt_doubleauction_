import React from 'react'
import PropTypes from 'prop-types'
import throttle from 'react-throttle-render'

import clone from 'clone'

import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardHeader from '@material-ui/core/CardHeader'

import { ReadJSON, InsertVariable, LineBreak } from '../util/ReadJSON'

const BidsTable = ({ buyerBids, sellerBids, deals, highestBid, lowestBid, expanded, dynamicText }) => {
  const rows = []
  const length = Math.max.apply(null, [buyerBids, sellerBids, deals].map(a => a.length))
  const maxValue = highestBid ? highestBid.bid : 0
  const minValue = lowestBid ? lowestBid.bid : 0
  const tableValue = (value) => {
    if (typeof value === 'undefined') {
      return ''
    } else {
      return value
    }
  }

  buyerBids = clone(buyerBids).sort((a, b) => b.bid - a.bid)
  sellerBids = clone(sellerBids).sort((a, b) => a.bid - b.bid)

  function get(map, key) {
    return map ? map[key] : null
  }
  for (let i = 0; i < length; i++) {
    rows.push(
      <tr key={`${get(buyerBids[i], 'id')}-${get(sellerBids[i], 'id')}-${get(deals[i], 'id')}`}>
        <td>{tableValue(get(buyerBids[i], 'bid'))}</td>
        <td>{tableValue(get(sellerBids[i], 'bid'))}</td>
        <td>{tableValue(get(deals[i], 'deal'))}</td>
      </tr>
    )
  }
  return (
    <Card
      initiallyExpanded={expanded}
    >
      <CardHeader
        title={
          <span>{LineBreak(InsertVariable(ReadJSON().static_text['table_title'], { buyer_num: buyerBids.length, seller_num: sellerBids.length, deals_num: deals.length, max_value: maxValue, min_value: minValue }, dynamicText['variables']))}</span>
        }
        actAsExpander={true}
        showExpandableButton={true}
      />
      <CardContent expandable={true}>
        <table>
          <thead>
            <tr>
              <th>{dynamicText['variables']['buying_price']}</th>
              <th>{dynamicText['variables']['selling_price']}</th>
              <th>{InsertVariable(ReadJSON().static_text['success_price'], {}, dynamicText['variables'])}</th>
            </tr>
          </thead>
          <tbody>
            {rows}
          </tbody>
        </table>
      </CardContent>
    </Card>
  )
}

BidsTable.propTypes = {
  buyerBids: PropTypes.array.isRequired,
  sellerBids: PropTypes.array.isRequired,
  deals: PropTypes.array.isRequired,
  highestBid: PropTypes.number.isRequired,
  lowestBid: PropTypes.number.isRequired,
  expanded: PropTypes.bool.isRequired,
  dynamicText: PropTypes.string.isRequired
}

export default throttle(BidsTable, 500)
