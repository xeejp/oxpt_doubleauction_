import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'

import Divider from '@material-ui/core/Divider'

import DealDialog from './DealDialog'
import BidsTable from './BidsTable'
import BidForm from './BidForm'

import { ReadJSON, LineBreak, InsertVariable } from '../util/ReadJSON'

const mapStateToProps = ({ personal, buyerBids, sellerBids, deals, highestBid, lowestBid, dynamicText }) =>
  Object.assign({}, personal, { buyerBids, sellerBids, deals, highestBid, lowestBid, dynamicText })

const Buyer = ({ money, bidded, bid, dealt, deal, dynamicText }) => {
  if (dealt) {
    return (
      <div>
        <DealDialog
          deal={deal}
          bid={bid}
          profit={money - deal}
        />
        <p>{InsertVariable(ReadJSON().static_text['success_text'], { deal: deal, bid: bid }, dynamicText['variables'])}</p>
        <p>{InsertVariable(ReadJSON().static_text['benefit'], { benefit: money - deal }, dynamicText['variables'])}</p>
      </div>
    )
  } else {
    return (
      <div>
        <p>{LineBreak(InsertVariable(dynamicText['your_buyer'], { money: money }, dynamicText['variables']))}</p>
        {bidded
          ? <p>{InsertVariable(ReadJSON().static_text['buyer_suggest'], { bid: bid }, dynamicText['variables'])}</p>
          : null
        }
        <BidForm />
      </div>
    )
  }
}

const Seller = ({ money, bidded, bid, dealt, deal, dynamicText }) => {
  if (dealt) {
    return (
      <div>
        <DealDialog
          deal={deal}
          bid={bid}
          profit={deal - money}
        />
        <p>{InsertVariable(ReadJSON().static_text['success_text'], { deal: deal, bid: bid }, dynamicText['variables'])}</p>
        <p>{InsertVariable(ReadJSON().static_text['benefit'], { benefit: deal - money }, dynamicText['variables'])}</p>
      </div>
    )
  } else {
    return (
      <div>
        <p>{LineBreak(InsertVariable(dynamicText['your_seller'], { money: money }, dynamicText['variables']))}</p>
        {bidded
          ? <p>{InsertVariable(ReadJSON().static_text['seller_suggest'], { bid: bid }, dynamicText['variables'])}</p>
          : null
        }
        <BidForm />
      </div>
    )
  }
}

const Auction = ({ buyerBids, sellerBids, deals, highestBid, lowestBid, role, money, bidded, bid, dealt, deal, dynamicText }) => (
  <div>
    <Card>
      <CardContent>
        {role === 'buyer' ? <Buyer money={money} bidded={bidded} bid={bid} dealt={dealt} deal={deal} dynamicText={dynamicText} /> : null}
        {role === 'seller' ? <Seller money={money} bidded={bidded} bid={bid} dealt={dealt} deal={deal} dynamicText={dynamicText} /> : null}
        {role === null ? <p>{ReadJSON().static_text['donot_join']}</p> : null}
      </CardContent>
    </Card>
    <Divider
      style={{
        marginTop: '5%',
      }}
    />
    <BidsTable
      buyerBids={buyerBids}
      sellerBids={sellerBids}
      deals={deals}
      highestBid={highestBid}
      lowestBid={lowestBid}
      expanded={true}
      dynamicText={dynamicText}
    />
  </div>
)

// TODO
Auction.propTypes = {
  buyerBids: PropTypes.array.isRequired,
  sellerBids: PropTypes.array.isRequired,
  deals: PropTypes.array.isRequired,
  highestBid: PropTypes.number.isRequired,
  lowestBid: PropTypes.number.isRequired,
  role: PropTypes.string.isRequired,
  money: PropTypes.number.isRequired,
  bidded: PropTypes.bool.isRequired,
  bid: PropTypes.number.isRequired,
  dealt: PropTypes.number.isRequired,
  deal: PropTypes.number.isRequired,
  dynamicText: PropTypes.string.isRequired
}

export default connect(mapStateToProps)(Auction)
