import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import BidsTable from './BidsTable'
import Chart from './Chart'

const mapStateToProps = ({ buyerBids, sellerBids, deals, users, exType, priceBase, priceInc, priceMax, priceMin, dynamicText }) => ({
  buyerBids,
  sellerBids,
  deals,
  users,
  exType,
  priceBase,
  priceInc,
  priceMax,
  priceMin,
  dynamicText
})

class Result extends Component {
  render() {
    const { buyerBids, sellerBids, deals, users, exType, priceBase, priceInc, priceMax, priceMin, dynamicText } = this.props
    return (
      <div>
        <BidsTable
          buyerBids={buyerBids}
          sellerBids={sellerBids}
          deals={deals}
          dynamicText={dynamicText}
        />
        <Chart
          users={users}
          deals={deals}
          expanded={true}
          ex_data={{ exType, priceBase, priceInc, priceMax, priceMin }}
          dynamicText={dynamicText}
        />
      </div>
    )
  }
}

Result.propTypes = {
  buyerBids: PropTypes.array.isRequired,
  sellerBids: PropTypes.array.isRequired,
  deals: PropTypes.array.isRequired,
  users: PropTypes.object.isRequired,
  exType: PropTypes.string.isRequired,
  priceBase: PropTypes.number.isRequired,
  priceInc: PropTypes.number.isRequired,
  priceMax: PropTypes.number.isRequired,
  priceMin: PropTypes.number.isRequired,
  dynamicText: PropTypes.string.isRequired
}

export default connect(mapStateToProps)(Result)
