import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import throttleProps from 'react-throttle-render'
import Dialog from '@material-ui/core/Dialog'
import Button from '@material-ui/core/Button'

import { ReadJSON, InsertVariable } from '../util/ReadJSON'

const mapStateToProps = ({ personal, dynamicText }) => ({
  deal: personal.deal,
  bid: personal.bid,
  profit: personal.profit,
  dynamicText: dynamicText
})

class DealDialog extends Component {
  constructor(props) {
    super(props)
    this.state = {
      open: true,
    }
  }

  handleOpen() {
    this.setState({ open: true })
  }

  handleClose() {
    this.setState({ open: false })
  }

  render() {
    const actions = [
      <Button
        label={ReadJSON().static_text['ok']}
        primary={true}
        onClick={this.handleClose.bind(this)}
      />
    ]
    return (
      <div>
        <Dialog
          title={ReadJSON().static_text['deal_success'][0]}
          actions={actions}
          modal={true}
          open={this.state.open}
        >
          <p>{InsertVariable(ReadJSON().static_text['deal_success'][1], { deal: this.props.deal }, this.props.dynamicText['variables'])}</p>
          <p>{InsertVariable(ReadJSON().static_text['deal_success'][2], { bid: this.props.bid }, this.props.dynamicText['variables'])}</p>
          <p>{InsertVariable(ReadJSON().static_text['deal_success'][3], { benefit: this.props.profit }, this.props.dynamicText['variables'])}</p>
          <p>{ReadJSON().static_text['deal_success'][4]}</p>
        </Dialog>
      </div>
    )
  }
}

DealDialog.propTypes = {
  deal: PropTypes.number.isRequired,
  bid: PropTypes.number.isRequired,
  profit: PropTypes.number.isRequired,
  dynamicText: PropTypes.string.isRequired
}

export default connect(mapStateToProps)(throttleProps(DealDialog))
