import { createAction } from 'redux-act'

export const fetchContents = createAction('fetch contents')
export const bid = createAction('BID', price => price)
