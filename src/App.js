import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { MuiThemeProvider } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardHeader from '@material-ui/core/CardHeader'
import CircularProgress from '@material-ui/core/CircularProgress'

import Auction from './components/Auction'
import Wait from './components/Wait'
import Description from './components/Description'
import Result from './components/Result'

import { ReadJSON, LineBreak } from './util/ReadJSON'

const mapStateToProps = ({ loading, mode }) => ({
  loading,
  mode
})

class App extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {}
  }

  componentDidMount() {
    sendData('fetch_contents')
  }

  render() {
    const { loading, mode } = this.props

    if (loading) {
      return (
        <Card style={{ padding: '20px' }}>
          <CardHeader title={ReadJSON().static_text['connecting'][0]} style={{ padding: '0px', marginTop: '7px', marginBottom: '14px' }}/>
          <CardContent style={{ padding: '0px', margin: '0px' }}>
            <div style={{ textAlign: 'center' }}>
              <CircularProgress style={{ margin: '0px', padding: '0px' }} />
            </div>
            <p style={{ margin: '0px', padding: '0px' }}>
              {LineBreak(ReadJSON().static_text['connecting'][0])}
            </p>
          </CardContent>
        </Card>
      )
    } else {
      return (
        <MuiThemeProvider>
          <div>
            { (mode === 'auction') ? <Auction /> : null }
            { (mode === 'wait') ? <Wait /> : null }
            { (mode === 'description') ? <Description /> : null }
            { (mode === 'result') ? <Result /> : null }
          </div>
        </MuiThemeProvider>
      )
    }
  }
}

App.propTypes = {
  loading: PropTypes.bool.isRequired,
  mode: PropTypes.string.isRequired
}

export default connect(mapStateToProps)(App)
