import 'regenerator-runtime/runtime'
import React from 'react'
import ReactDOM from 'react-dom'
import { createStore, applyMiddleware, compose } from 'redux'
import { Provider } from 'react-redux'
import createSagaMiddleware from 'redux-saga'
import { createLogger } from 'redux-logger'
import channel from 'oxpt'
import saga from './saga'
import reducer from './reducer'
import App from './App'

const sagaMiddleware = createSagaMiddleware()
let middlewares = [
  sagaMiddleware
]
if (process.env.NODE_ENV !== 'production') {
  middlewares = [
    ...middlewares,
    createLogger({ collapsed: true, diff: true })
  ]
}
const composeEnhancers =
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
    : compose

const enhancer = composeEnhancers(
  applyMiddleware(...middlewares)
)

const store = createStore(reducer, enhancer)
sagaMiddleware.run(saga)
channel.join()

const root = document.createElement('div')
document.body.appendChild(root)

channel.on('update', (payload) => {
  store.dispatch({ type: payload.type, data: payload.data })
})

window.sendData = function sendData(event, payload) {
  channel.push('input', { event: event, payload: payload })
}

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  root
)
