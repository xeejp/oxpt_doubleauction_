import { createReducer } from 'redux-act'

import { ReadJSON } from './util/ReadJSON'

const reducer = createReducer(
  {
    'RECEIVE_CONTENTS': (_, contents) => contents.dynamicText ? contents : Object.assign(contents, { dynamicText: ReadJSON().dynamicText }),
    'update contents': () => ({ loading: false }),
  }, {
    loading: true,
    mode: 'waiting',
    buyerBids: [],
    sellerBids: [],
    highestBid: null,
    lowestBid: null,
    deals: [],
    personal: {
    },
    dynamicText: null,
  }
)

export default reducer
